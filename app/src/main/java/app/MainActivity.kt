package app

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import josercl.android.library.galleryviewer.GalleryViewer

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)

        setSupportActionBar(toolbar)

        val gallery = findViewById<GalleryViewer>(R.id.gallery)
        gallery.setImages((0 until 30).map {
            "https://picsum.photos/id/$it/1080/1920"
        })
    }

}
