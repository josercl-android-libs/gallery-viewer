package josercl.android.library.galleryviewer.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.view.doOnLayout
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import coil.size.Scale
import coil.transform.Transformation
import josercl.android.library.galleryviewer.GalleryViewer
import josercl.android.library.galleryviewer.R

class GalleryViewAdapter(
    @RecyclerView.Orientation val orientation: Int,
    private val layout: GalleryViewer.Layout,
    val onSelectImage: (View, ImageView, List<String>, Int) -> Unit
) : ListAdapter<String, GalleryViewAdapter.GalleryViewHolder>(
    object : DiffUtil.ItemCallback<String>() {
        override fun areItemsTheSame(oldItem: String, newItem: String): Boolean = oldItem == newItem

        override fun areContentsTheSame(oldItem: String, newItem: String): Boolean =
            oldItem == newItem
    }
) {
    var transformations: List<Transformation> = emptyList()

    fun loadTransformations(transformations: List<Transformation>) {
        this.transformations = transformations
        notifyItemRangeChanged(0, itemCount, "transformations")
    }

    override fun getItemViewType(position: Int): Int {
        if (orientation == RecyclerView.VERTICAL) {
            return when (layout) {
                GalleryViewer.Layout.GRID -> R.layout.item_gallery_grid_vertical
                else -> R.layout.item_gallery_linear_vertical
            }
        }
        return when (layout) {
            GalleryViewer.Layout.GRID -> R.layout.item_gallery_grid_horizontal
            else -> R.layout.item_gallery_linear_horizontal
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GalleryViewHolder {
        return GalleryViewHolder(
            LayoutInflater.from(parent.context).inflate(viewType, parent, false)
        )
    }

    override fun onBindViewHolder(holder: GalleryViewHolder, position: Int) {
        holder.bindTo(getItem(position))
    }

    inner class GalleryViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val image = itemView.findViewById<ImageView>(R.id.image)

        init {
            itemView.setOnClickListener {
                onSelectImage(it, image, currentList, adapterPosition)
            }
        }

        fun bindTo(item: String) {
            itemView.doOnLayout {
                if (it.width > 0 && it.height > 0) {
                    image.load(item) {
                        placeholder(R.drawable.galleryviewer_default_placeholder)
                        size(it.width, it.height)
                        scale(Scale.FILL)
                        crossfade(500)
                        transformations(this@GalleryViewAdapter.transformations)
                    }
                }
            }
        }
    }
}