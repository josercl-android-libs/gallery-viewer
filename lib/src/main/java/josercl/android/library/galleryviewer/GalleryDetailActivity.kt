package josercl.android.library.galleryviewer

import android.app.DownloadManager
import android.content.Context
import android.content.Intent
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.MenuItem
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.PermissionChecker
import androidx.core.net.toUri
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.viewpager2.widget.ViewPager2
import coil.Coil
import coil.api.load
import com.google.android.material.appbar.MaterialToolbar
import josercl.android.library.galleryviewer.adapter.GalleryDetailAdapter
import java.util.*

class GalleryDetailViewModel : ViewModel() {
    val currentItem = MutableLiveData<Int>().apply { value = 0 }
    val images = MutableLiveData<List<String>>()

    val currentImage = MediatorLiveData<String>().apply {
        addSource(images) {
            if (it != null && it.isNotEmpty()) {
                this.value = it[currentItem.value!!]
            }
        }
        addSource(currentItem) {
            if (images.value != null && images.value!!.isNotEmpty()) {
                this.value = images.value!![currentItem.value!!]
            }
        }
    }
}

class GalleryDetailActivity : AppCompatActivity() {
    private val viewModel: GalleryDetailViewModel by viewModels()

    private lateinit var pager: ViewPager2
    private lateinit var toolbar: MaterialToolbar
    private lateinit var totalItemsText: TextView
    private lateinit var titleText: TextView
    private lateinit var currentItemText: TextView
    private lateinit var downloadButton: TextView
    private lateinit var shareButton: TextView

    companion object {
        fun getIntent(
            context: Context,
            title: String,
            images: List<String>,
            currentImage: Int
        ): Intent {
            return Intent(context, GalleryDetailActivity::class.java).apply {
                putExtra("ARG_TITLE", title)
                putExtra("ARG_IMAGES", images.toTypedArray())
                putExtra("ARG_CURRENT_IMAGE", currentImage)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gallery_detail)

        pager = findViewById(R.id.images_pager)
        toolbar = findViewById(R.id.toolbar)
        totalItemsText = findViewById(R.id.total_items)
        currentItemText = findViewById(R.id.current_item)
        titleText = findViewById(R.id.title)
        downloadButton = findViewById(R.id.download_button)
        shareButton = findViewById(R.id.share_button)
        pager = findViewById(R.id.images_pager)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        title = intent.extras!!.getString("ARG_TITLE")

        currentItemText.setTextColor(titleText.currentTextColor)
        totalItemsText.setTextColor(titleText.currentTextColor)

        if (viewModel.images.value == null) {
            viewModel.images.value = intent.extras!!.getStringArray("ARG_IMAGES")!!.toList()
        }
        if (viewModel.currentItem.value!! == 0) {
            viewModel.currentItem.value = intent.extras!!.getInt("ARG_CURRENT_IMAGE")
        }

        viewModel.images.observe(this, Observer {
            if (it != null) {
                totalItemsText.text = "${it.size}"
                if (pager.adapter == null) {
                    initPager(it)
                }
            }
        })

        viewModel.currentItem.observe(this, Observer {
            currentItemText.text = (it + 1).toString()
            pager.setCurrentItem(viewModel.currentItem.value!!, true)
        })

        viewModel.currentImage.observe(this, Observer {})

        downloadButton.setOnClickListener { startDownload() }
        shareButton.setOnClickListener { startShare() }
    }

    override fun setTitle(title: CharSequence?) {
        super.setTitle(title)
        titleText.text = title
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun initPager(list: List<String>) {
        pager.adapter = GalleryDetailAdapter(
            list,
            longClickListener = {
                val layout = findViewById<MotionLayout>(R.id.layout)
                if (layout.currentState != layout.endState) {
                    layout.transitionToEnd()
                } else {
                    layout.transitionToStart()
                }
            }
        )

        pager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                viewModel.currentItem.value = position
            }
        })
        pager.setCurrentItem(viewModel.currentItem.value!!, false)
    }

    private fun startDownload() {
        val downloadManager = getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
        val request = DownloadManager.Request(viewModel.currentImage.value!!.toUri())
            .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
        downloadManager.enqueue(request)
    }

    private fun startShare() {
        if (hasPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            saveTempImage {
                it?.run {
                    val sendIntent = Intent(Intent.ACTION_SEND).apply {
                        type = "image/*"
                        putExtra(Intent.EXTRA_STREAM, this@run)
                    }
                    startActivityForResult(
                        Intent.createChooser(
                            sendIntent,
                            getString(R.string.galleryviewer_share_title)
                        ),
                        2000
                    )
                }
            }
        } else {
            ActivityCompat.requestPermissions(
                this@GalleryDetailActivity,
                arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE),
                1000
            )
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == 1000 &&
            grantResults.isNotEmpty() &&
            grantResults.all { it == PermissionChecker.PERMISSION_GRANTED }
        ) {
            startShare()
        }
    }

    private fun saveTempImage(callback: (Uri?) -> Unit) {
        Coil.load(this, viewModel.currentImage.value!!) {
            target(
                onSuccess = {
                    val bitmap = (it as BitmapDrawable).bitmap
                    val path = MediaStore.Images.Media.insertImage(
                        contentResolver,
                        bitmap,
                        "${UUID.randomUUID()}",
                        null
                    )
                    callback(path.toUri())
                },
                onError = {
                    callback(null)
                }
            )
        }
    }
}
