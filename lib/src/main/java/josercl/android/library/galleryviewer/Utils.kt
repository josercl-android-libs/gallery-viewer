package josercl.android.library.galleryviewer

import android.content.Context
import android.content.res.Resources
import android.util.TypedValue
import androidx.core.content.PermissionChecker

fun Float.toDp() = TypedValue.applyDimension(
    TypedValue.COMPLEX_UNIT_DIP,
    this,
    Resources.getSystem().displayMetrics
)

fun Context.hasPermission(permission: String) =
    PermissionChecker.checkSelfPermission(this, permission) == PermissionChecker.PERMISSION_GRANTED