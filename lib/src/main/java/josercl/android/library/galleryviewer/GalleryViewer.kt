package josercl.android.library.galleryviewer

import android.content.Context
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.util.AttributeSet
import androidx.core.app.ActivityOptionsCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import coil.transform.Transformation
import josercl.android.library.galleryviewer.adapter.GalleryViewAdapter
import josercl.android.library.galleryviewer.decoration.SpacerItemDecoration

class GalleryViewer @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    private val defStyleAttr: Int = 0
) : RecyclerView(context, attrs, defStyleAttr) {

    enum class Layout {
        GRID,
        LINEAR
    }

    private val defaultLayout = Layout.GRID
    private val defaultItemMargin = 16f.toDp()

    var layout = defaultLayout
    var itemMargin = defaultItemMargin
    var title = ""

    fun applyTransformations(transformations: List<Transformation> = emptyList()) {
        (adapter as GalleryViewAdapter).loadTransformations(transformations)
    }

    init {
        saveAttributes(attrs)

        val properties = LayoutManager.getProperties(context, attrs, defStyleAttr, 0)
        val orientation = properties.orientation
        val reversed = properties.reverseLayout
        val columns = properties.spanCount

        addItemDecoration(SpacerItemDecoration(itemMargin, itemMargin, layout, orientation))

        this.layoutManager = when (layout) {
            Layout.GRID -> GridLayoutManager(context, columns, orientation, reversed)
            Layout.LINEAR -> LinearLayoutManager(context, orientation, reversed)
        }

        this.adapter =
            GalleryViewAdapter(orientation, layout) { view, imageView, list, index ->
                var options: Bundle? = null
                val bitmapDrawable = imageView.drawable as? BitmapDrawable

                bitmapDrawable?.run {
                    options =
                        ActivityOptionsCompat.makeThumbnailScaleUpAnimation(
                            view,
                            this.bitmap,
                            view.x.toInt(),
                            view.y.toInt()
                        ).toBundle()
                }

                context.startActivity(
                    GalleryDetailActivity.getIntent(context, title, list, index),
                    options
                )
            }
    }

    private fun saveAttributes(attrs: AttributeSet?) {
        attrs?.run {
            val attributes = context.obtainStyledAttributes(
                attrs, R.styleable.GalleryViewer, defStyleAttr, 0
            )
            itemMargin = attributes.getDimension(
                R.styleable.GalleryViewer_galleryviewer_itemMargin,
                defaultItemMargin
            )
            layout = Layout.values()[attributes.getInteger(
                R.styleable.GalleryViewer_galleryviewer_layout,
                0
            )]
            title = attributes.getString(R.styleable.GalleryViewer_galleryviewer_title) ?: ""

            attributes.recycle()
        }
    }

    fun setImages(images: List<String>) = (this.adapter as GalleryViewAdapter).submitList(images)

}