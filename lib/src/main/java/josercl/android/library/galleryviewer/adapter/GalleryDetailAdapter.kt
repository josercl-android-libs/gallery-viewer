package josercl.android.library.galleryviewer.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import josercl.android.library.galleryviewer.R

class GalleryDetailAdapter(
    private val list: List<String>,
    val longClickListener: () -> Unit
) :
    RecyclerView.Adapter<GalleryDetailAdapter.GalleryDetailItemHolder>(){

    override fun getItemViewType(position: Int): Int = R.layout.item_gallery_detail

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GalleryDetailItemHolder {
        return GalleryDetailItemHolder(
            LayoutInflater.from(parent.context).inflate(viewType, parent, false)
        )
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: GalleryDetailItemHolder, position: Int) =
        holder.bindTo(list[position])

    inner class GalleryDetailItemHolder(view: View): RecyclerView.ViewHolder(view) {
        private val image: ImageView = itemView.findViewById(R.id.image)
        private val progressBar: ProgressBar = itemView.findViewById(R.id.progress)
        private val layout: ConstraintLayout = itemView.findViewById(R.id.layout)

        fun bindTo(s: String) {
            layout.setOnLongClickListener {
                longClickListener()
                true
            }
            image.load(s) {
                crossfade(500)
                key(s)
                listener(
                    onStart = {
                        progressBar.isVisible = true
                    },
                    onSuccess = { _, _ ->
                        progressBar.isVisible = false
                    },
                    onCancel = {
                        progressBar.isVisible = false
                    },
                    onError = { _, error ->
                        progressBar.isVisible = false
                        Log.e("GalleryViewer", error.message, error)
                        Toast.makeText(itemView.context, error.message, Toast.LENGTH_LONG)
                            .show()
                    }
                )
            }
        }
    }
}