package josercl.android.library.galleryviewer.decoration

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.State
import josercl.android.library.galleryviewer.GalleryViewer

class SpacerItemDecoration(
    vertical: Float,
    horizontal: Float,
    private val mode: GalleryViewer.Layout,
    @RecyclerView.Orientation private val orientation: Int
) : RecyclerView.ItemDecoration() {

    private val verticalSpaceInPixels = vertical.toInt()
    private val horizontalSpaceInPixels = horizontal.toInt()

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: State) {
        val position = parent.getChildAdapterPosition(view)
        if (mode == GalleryViewer.Layout.GRID) {
            val spanCount = (parent.layoutManager as GridLayoutManager).spanCount

            if (orientation == RecyclerView.VERTICAL) {
                if (position < spanCount) {
                    outRect.top += verticalSpaceInPixels
                }
                outRect.bottom += verticalSpaceInPixels

                outRect.left = horizontalSpaceInPixels / 2
                outRect.right = horizontalSpaceInPixels / 2
            } else {
                if (position < spanCount) {
                    outRect.left += horizontalSpaceInPixels
                }
                outRect.right += horizontalSpaceInPixels

                outRect.top = verticalSpaceInPixels / 2
                outRect.bottom = verticalSpaceInPixels / 2
            }
        } else {
            if (orientation == RecyclerView.VERTICAL) {
                if (position == 0) {
                    outRect.top = verticalSpaceInPixels
                }
                outRect.bottom = verticalSpaceInPixels
                outRect.right = horizontalSpaceInPixels
                outRect.left = horizontalSpaceInPixels
            } else {
                if (position == 0) {
                    outRect.left = horizontalSpaceInPixels
                }
                outRect.right = horizontalSpaceInPixels
                outRect.top = verticalSpaceInPixels
                outRect.bottom = verticalSpaceInPixels
            }
        }
    }
}